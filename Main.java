import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static final String FRAME_CHARS = "+ ";
    public static final String BOXES_CHARS = "# ";
    public static final String DESTROYED_CHARS = "  ";


    public interface NeighbourDestroyedListener {
        void destroyedNeighbour(NeighbourDestroyedListener senderNeighbour);
    }

    public static class Box implements NeighbourDestroyedListener {
        private boolean isDestroyed = false;
        private ArrayList<NeighbourDestroyedListener> neighbourBoxListeners = new ArrayList<>();
        private boolean isFrame;

        public Box() {
                this(false);
            }

        public Box(boolean isFrame){
                this.isFrame = isFrame;
            }

        public boolean addNeighbourListener(NeighbourDestroyedListener neighbourDestroyedListener){
            if(neighbourDestroyedListener == null)
                return false;

            if(!neighbourBoxListeners.contains(neighbourDestroyedListener)){
                neighbourBoxListeners.add(neighbourDestroyedListener);
               return true;
            }
            return false;
        }

        @Override
        public void destroyedNeighbour(NeighbourDestroyedListener sender) {
            //Do not repeat if destroyed
            //Do not do anything if frame
            if(!isDestroyed && !isFrame){
                neighbourBoxListeners.remove(sender);
                if(shouldBeDestroyed()){
                    destroy();
                }
            }
        }

        public boolean destroy() {
            if (isFrame)
               return false;

            //Set destroyed and notify neighbours cube isDestroyed
            isDestroyed = true;
            neighbourBoxListeners.forEach(neighbourDestroyedListener -> {
                neighbourDestroyedListener.destroyedNeighbour(this);
            });
            return true;
        }

        //Cube is destroyed if not frame and there are not at least 2 neighbour blocks to keep it
        private boolean shouldBeDestroyed(){
            if(neighbourBoxListeners.size() < 3 && !isFrame){
                return true;
            }
            return false;
        }

        public boolean isDestroyed(){
            return isDestroyed;
        }
    }

    public static void main(String[] args) {
        System.out.println("Welcome to \n\n .::ICE BREAKER::. \n");
        System.out.println("/***HOW TO PLAY***\\\nThe game is really simple - you must make your opponent destroy all ice cubes." +
                "\nEach player chooses a block to destroy. If a block has less than 3 neighbouring blocks it becomes unstable and is destroyed automatically." +
                "\nMay the force be with you! Good luck!\n********************\n");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enjoy the game :)\n\nInput game size: ");
        int gameSize = 0;

        boolean gameSizeValid = false;
        while(!gameSizeValid){
            try{
                //Unplayable if smaller than 4 because it includes frame
                gameSize = scanner.nextInt() + 2;
                if(gameSize >= 4){
                    gameSizeValid = true;
                }else{
                    throw new Exception("");
                }
            }
            catch (Exception e){
                System.out.println("Wrong format. Try again...");
                scanner.nextLine();
            }
        }
        scanner.nextLine();

        Box[][] gameBoard = new Box[gameSize][gameSize];

        //Initialize game
        for (int i = 0; i < gameSize; i++) {
            for (int j = 0; j < gameSize; j++){
                if(i == 0 || j == 0 || i == gameSize-1 || j == gameSize-1){
                    gameBoard[i][j] = new Box(true);
                    System.out.print(FRAME_CHARS);
                }else {
                    gameBoard[i][j] = new Box();
                    System.out.print(BOXES_CHARS);
                }
            }
            System.out.println("");
        }
        //Set neighbours
        for (int i = 0; i < gameSize; i++) {
            for (int j = 0; j < gameSize; j++) {
                NeighbourDestroyedListener upListener;
                NeighbourDestroyedListener downListener;
                NeighbourDestroyedListener rightListener;
                NeighbourDestroyedListener leftListener;
                try {
                    upListener = gameBoard[i - 1][j];
                } catch (Exception e) {
                    upListener = null;
                }
                try {
                    downListener = gameBoard[i + 1][j];
                } catch (Exception e) {
                    downListener = null;
                }
                try {
                    rightListener = gameBoard[i][j + 1];
                } catch (Exception e) {
                    rightListener = null;
                }
                try {
                    leftListener = gameBoard[i][j - 1];
                } catch (Exception e) {
                    leftListener = null;
                }

                gameBoard[i][j].addNeighbourListener(upListener);
                gameBoard[i][j].addNeighbourListener(downListener);
                gameBoard[i][j].addNeighbourListener(leftListener);
                gameBoard[i][j].addNeighbourListener(rightListener);
            }
        }
            //Define player 1 or player 2
            int playersTurn = 1;

            //Game loop
            boolean gameFinished = false;
            while (!gameFinished){

                System.out.printf("Player %d which box to destroy ? [col] [row] \n",playersTurn%2 == 0 ? 2 : 1 );

                //Chosen blocks validation
                String[] rowAndColUnparsed = scanner.nextLine().trim().split("\\s+");
                int[] rowAndColParsed = new int[2];
                try{
                    //First block [1][1] because [0][0] is frame
                    rowAndColParsed[0] = Integer.parseInt(rowAndColUnparsed[0]);
                    rowAndColParsed[1] = Integer.parseInt(rowAndColUnparsed[1]);
                    }
                    catch (Exception e){
                        System.out.println("Wrong format. Try again...");
                        continue;
                }

                //Try to destroy selected block
                try{
                    if(!gameBoard[rowAndColParsed[1]][rowAndColParsed[0]].destroy()){
                        throw new Exception("Unable to destroy block.");
                    }else {
                        playersTurn++;
                    };
                }catch (Exception e){
                    System.out.println("Already destroyed, frame or not existing ice block. Try again...");
                    continue;
                }

                //Refresh gameboard
                boolean shouldEndAlready = true;
                for(int m = 0; m < gameBoard.length; m++){
                    for(int n = 0; n < gameBoard.length; n++){
                        if(gameBoard[m][n] == null || gameBoard[m][n].isDestroyed){
                                gameBoard[m][n] = null;
                                System.out.print(DESTROYED_CHARS);
                        }else if(gameBoard[m][n].isFrame){
                            System.out.print(FRAME_CHARS);
                        }else{
                            if(shouldEndAlready)
                                shouldEndAlready = false;
                            System.out.print(BOXES_CHARS);
                        }
                    }
                    System.out.println("");
                }

                //End game
                if(shouldEndAlready){
                    System.out.println("You lost!");
                    System.out.printf("\nPlayer %d won!",playersTurn%2 == 0 ? 2 : 1);
                    gameFinished = true;
                }
            }
        }
    }


